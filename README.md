# Carrefour des innovations sociales

Application ReactJS moteur de recherche des innovations sociales.

Dépendances :

* [material-ui v1](http://material-ui-next.com/) 
* [leaflet](http://leafletjs.com)
* [autosuggest-highlight](https://www.npmjs.com/package/autosuggest-highlight)
* [throttle-debounce](https://www.npmjs.com/package/throttle-debounce)

## Installation

Installer npm, puis lancer la commande `npm install` pour installer les dépendances. 

## Développement

Les scripts npm sont définis dans le fichier `package.json`. Pour lancer un serveur de développement, lancer la commande 

    npm start

## Déploiement

Pendant le développement, le projet est déployé sur surge.sh, à l'adresse [mc-cget.surge.sh](http://mc-cget.surge.sh/)

    npm run build # des opérations permettant de déployer sur surge sont effectuées après le build
    cd build/
    surge
