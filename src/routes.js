import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Route } from 'react-router-dom';

import Home from './components/Home/';
import SearchProjectApp from './components/SearchProjectApp';
import About from './components/About';
import Sourceurs from './components/Sourceurs';

/* Routes for CGET App */

export const routes = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/rechercher',
    component: SearchProjectApp,
  },
  {
    path: '/apropos',
    component: About,
  },
  {
    path: '/contributeurs',
    component: Sourceurs,
  },
];

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
export const RouteWithSubRoutes = route => (
  <Route
    exact
    path={route.path}
    render={props => (
    // pass the sub-routes down to keep nesting
      <route.component {...props} routes={route.routes} />
  )}
  />
);


class ScrollRestoration extends Component {
  componentDidUpdate (prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render () {
    return this.props.children;
  }
}

export default withRouter(ScrollRestoration);
