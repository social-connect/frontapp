import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import { lightGreen, blueGrey } from 'material-ui/colors';
import Button from 'material-ui/Button';

import ProjectsViewGrid from '../Projects/ProjectsViewGrid/';
import topprojects from '../../data/topprojects.json';



const styles = {
  appTitle : {
    color: lightGreen[600],
  },
  appSlogan: {
    color: blueGrey[500],
  },
  TextContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'baseline',
    alignContnent: 'space-around',
    marginLeft:'auto',
    marginRight:'auto',
    textAlign: 'left'
  },
  columnText: {
    width: '40%',
    marginLeft:'auto',
    marginRight:'auto',
  },
  headline: {
    color: lightGreen[700],
    marginTop:'20px',
    textAlign: 'center'
  },
  searchLink: {
    margin:'20px',
    textAlign: 'center'
  },
  link: {
    textDecoration: 'none'
  },
  Title: {
    fontSize: '1.4em'
  }
}

class Home extends Component {
  state= {
    gridView: true,
    selectedProject: {}
  }

  handleSelectedProjectChange = (selectedProject) => {
    this.setState({
      selectedProject: selectedProject
    });
  }

  render() {

    const { classes } = this.props;

    return(
      <div className='container'>
        <Typography className={classes.headline} type="headline">
          Le carrefour des innovations sociales vise à rendre accessible, de manière fiable et mise à jour, <br />l’ensemble des innovations sociales déjà recensées en ligne
        </Typography>
        <div className={classes.TextContainer}>
          <div className={classes.columnText}>
            <h3 id="quels-sont-ses-objectifs">Quels sont ses objectifs ?</h3>
              <p>Mettre en commun et valoriser les innovations sociales ! Le carrefour des innovations sociales vise à :</p>
              <ul>
                <li>recenser et mettre en visibilité les innovations sociales déjà capitalisées. Il s’agit de fournir une information tendant vers l’exhaustivité, fiable et mise à jour des projets d’innovation sociale ;</li>
                <li>reconnaitre et libérer le potentiel inspirant des innovations sociales ;</li>
                <li>renforcer les écosystèmes d’innovation sociale, dans les territoires ainsi qu’au niveau national et être un outil d'appui et de plaidoyer pour le développement de l'innovation sociale dans le système économique et les politiques publiques.</li>
              </ul>
            </div>
            <div className={classes.columnText}>
              <h3 id="qui-y-participe-et-où-en-sommes-nous">Qui y participe et où en sommes-nous ?</h3>
                <p>Aujourd’hui près de 70 partenaires (Ministères, associations d’élus, institutions publiques, associations, têtes de réseaux, fondations, collectifs citoyens) sont parties prenantes actives du projet. La coordination du projet est assurée dans un premier temps par le Commissariat Général à l’Egalité des Territoires et La Fonda, puis à terme par la structure porteuse du collectif et les instances de gouvernance qu’elle se sera données.</p>
                <p>Pour l’instant, seule cette maquette web est opérationnelle. A partir de janvier 2018, deux Entrepreneurs d’intérêt général viendront rejoindre l’équipe projet pour assurer le développement informatique complet de la plateforme web.</p>
            </div>
        </div>
        <div className={classes.container}>
          <div className={classes.infoResultBar}>
            <h2 className={classes.infosGen}>Derniers projets</h2>
              <Grid item className="ResultsActions">
                <Grid className={classes.containerResult} container spacing={24}>
                  <Grid item xs={12}>
                  <ProjectsViewGrid
                      projects={topprojects}
                      selectedProject={this.state.selectedProject}
                      onSelectProject={this.handleSelectedProjectChange}
                    />
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
        <div className={classes.searchLink} >
          <Link to="/rechercher" className={classes.link}>
            <Button raised>
              <Typography className={classes.Title} type="headline">
                Rechercher d'autres projets
              </Typography>
            </Button>
          </Link>
        </div>
      </div>
    )
  }
}
export default withStyles(styles)(Home);
