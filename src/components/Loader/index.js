import React, { Component } from 'react';

import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
  progressContainer: {
    height: '600px',
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center',
    marginTop: '25%',
    marginBottom: '25%',
  },
  progress: { margin: `0 ${theme.spacing.unit * 2}px` },
});

class Loader extends Component {
  render () {
    const { classes } = this.props;
    return (
      <div className={classes.progressContainer}>
        <CircularProgress className={classes.progress} size={50} color="primary" />
      </div>
    );
  }
}
export default withStyles(styles)(Loader);
