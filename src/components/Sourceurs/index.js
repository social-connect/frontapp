import React, { Component } from 'react';

import { sourceurs } from '../../assets/config.json';
import SourceursContent from './SourceursContent';


class Sourceurs extends Component {
  render () {
    return (
      <div className="container">
        <SourceursContent sourceurs={sourceurs} />
      </div>
    );
  }
}


export default Sourceurs;
