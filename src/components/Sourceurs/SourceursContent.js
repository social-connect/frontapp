import React from 'react';

import { List, ListItem } from 'material-ui';

function AboutContent (props) {
  const { sourceurs } = props;

  return (

    <div>
      <p>Ils recensent des projets d’innovations sociale et contribuent au Carrefour des innovations sociales :</p>
      <List>
        {sourceurs.map((sourceur, i) =>
          sourceur.label !== '' && <ListItem key={`sourceurlist-${i}`}>{sourceur.label}</ListItem>)}
      </List>
    </div>
  );
}

export default AboutContent;
