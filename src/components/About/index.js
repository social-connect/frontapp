import React, { Component } from 'react';

import AboutContent from './AboutContent';


class About extends Component {
  render () {
    return (
      <div className="container">
        <AboutContent />
      </div>
    );
  }
}
export default About;
