import React from 'react';
import partenaires from '../../assets/partenaires.png';

function AboutContent () {
  return (

    <div>
      <h3 id="doù-vient-le-carrefour-des-innovation-sociales">D’où vient le Carrefour des innovation sociales ?</h3>
      <p>Le projet de carrefour des innovations sociales est né d’un double constat :</p>
      <ul>
        <li>de riches gisements de projets socialement innovants existent dans les territoires, dans tous les domaines et sont portés par des acteurs divers (associations, acteurs privés, publics, partenariats hybrides…) ;</li>
        <li>une grande diversité d’acteurs contribue à les valoriser, notamment via des plateformes web.</li>
      </ul>
      <p>Il en résulte, pour le grand public, comme pour les acteurs institutionnels ou les porteurs de projets, un enjeu de visibilité nationale et territoriale des innovations sociales afin de soutenir leur capacité à essaimer, changer d’échelle ou inspirer d’autres initiatives.</p>
      <p>Forts de ce bilan, des acteurs divers se sont rassemblés en 2016 au sein d’un collectif pour mutualiser les fruits de leurs capitalisations et développer un nouveau mode d’accès à leur contenu, qui favorise la mise en lien, le partage de connaissances et donne à voir leur complémentarité. Le collectif est né par ailleurs du besoin de coopération et d’amélioration collective des pratiques de capitalisation. Le collectif du carrefour des innovations sociales s’appuie sur un outil : une plateforme web hébergeant un moteur de recherche partagé.</p>

      <h3 id="quels-sont-ses-objectifs">Quels sont ses objectifs ?</h3>
      <p>Mettre en commun et valoriser les innovations sociales ! Le carrefour des innovations sociales vise à :</p>
      <ul>
        <li>recenser et mettre en visibilité les innovations sociales déjà capitalisées. Il s’agit de fournir une information tendant vers l’exhaustivité, fiable et mise à jour des projets d’innovation sociale ;</li>
        <li>reconnaitre et libérer le potentiel inspirant des innovations sociales ;</li>
        <li>renforcer les écosystèmes d’innovation sociale, dans les territoires ainsi qu’au niveau national et être un outil d'appui et de plaidoyer pour le développement de l'innovation sociale dans le système économique et les politiques publiques.</li>
      </ul>

      <h3 id="qui-y-participe-et-où-en-sommes-nous">Qui y participe et où en sommes-nous ?</h3>
      <p>Aujourd’hui près de 70 partenaires (Ministères, associations d’élus, institutions publiques, associations, têtes de réseaux, fondations, collectifs citoyens) sont parties prenantes actives du projet. La coordination du projet est assurée dans un premier temps par le Commissariat Général à l’Egalité des Territoires et La Fonda, puis à terme par la structure porteuse du collectif et les instances de gouvernance qu’elle se sera données.</p>
      <p>Pour l’instant, seule cette maquette web est opérationnelle. A partir de janvier 2018, deux Entrepreneurs d’intérêt général viendront rejoindre l’équipe projet pour assurer le développement informatique complet de la plateforme web.</p>
      <h3 id="valeurs-du-projet-une-gouvernance-collective-pour-un-projet-conçu-comme-un-commun">Valeurs du projet : une gouvernance collective pour un projet conçu comme un commun</h3>
      <p>Le projet de carrefour des innovations sociales est structuré autour de quatre valeurs fondamentales :</p>
      <ul>
        <li>la valorisation et la reconnaissance de l’expertise de chaque partenaire s’inscrivant dans une dynamique de confiance ainsi que le respect de chaque partenaire et des porteurs de projet concernés par les contenus diffusés ;</li>
        <li>une approche ouverte de l’innovation sociale dans les territoires. Chaque partenaire est garant du caractère innovant des solutions concrètes, utiles, conduites en réponse à des problématiques territoriales. Le collectif du carrefour des innovations sociales s’appuie sur l’expertise d’usage de chaque partenaire suivant une logique de bienveillance ;</li>
        <li>une approche d’open data et d’open source : la plateforme fait office de catalyseur de nouveaux échanges de pratiques et de savoir-faire se plaçant en complémentarité d’initiatives existantes. Chaque utilisateur de la plateforme a la capacité de contacter les porteurs de projets capitalisés, les partenaires qui les ont capitalisés ou les coordinateurs du collectif du carrefour des innovations sociales. La plateforme web s’appuie sur des outils développés en open source. Elle est développée selon une approche utilisateurs, en mode agile ;</li>
        <li>la gestion du projet suivant les principes des communs : les données issues du travail de capitalisation sont une ressource partagée, que la plateforme permettra de valoriser et diffuser collectivement, tout en reconnaissant l’apport de chacun.</li>
      </ul>
      <img className="img" src={partenaires} alt="répartition des partenaires" />

      <h3 id="comment-le-projet-est-il-organisé">Comment le projet est-il organisé ?</h3>
      <p>Le Carrefour des innovations sociales s’organise selon deux principes de gouvernance :</p>
      <ul>
        <li>une logique d’égalité, dans la gouvernance du projet selon le principe « un partenaire, une voix » ;</li>
        <li>une gouvernance partagée du collectif et de la plateforme web à laquelle les partenaires contribuent.</li>
      </ul>

      <p>Pour plus d’informations, contactez <a href="mailto:benedicte.pachod@cget.gouv.fr">Bénédicte Pachod</a>, au CGET ou <a href="mailto:bastien.engelbach@fonda.asso.fr">Bastien Engelbach</a> de La Fonda.</p>

    </div>
  );
}

export default AboutContent;
