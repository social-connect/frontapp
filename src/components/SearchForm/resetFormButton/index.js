import React, { Component } from 'react';

import IconButton from 'material-ui/IconButton';
import Clear from 'material-ui-icons/Clear';

class ResetFormButton extends Component {

  changeInputValue = (e) => {
    this.props.clearInputValue(e.target.value)
  }

  render(){

    return(
        <IconButton
          onClick={this.changeInputValue}
          style={{position: 'absolute', right:'12px', bottom:'-7px'}}
          aria-label="effacer le filtre"
        >
          <Clear />
        </IconButton>
    )
  }
}
export default ResetFormButton;
