import React, { Component } from 'react';

import Checkbox from 'material-ui/Checkbox';
import { FormLabel, FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import { lightGreen } from 'material-ui/colors';

const styles = theme => ({
  controlCheck: {
    flex: 'inline-flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  ckeckbox: { color: lightGreen[600] },
  label: { color: lightGreen[600] },
});

class SearchMedia extends Component {
  render () {
    const classes = this.props.classes;

    return (
      <FormControl className={classes.controlCheck}>
        <FormLabel className={classes.label} component="legend">Afficher les projets avec vidéo</FormLabel>
        <Checkbox
          className={classes.ckeckbox}
          checked={this.props.checkboxMedia}
          onChange={this.props.handleFilterMediaChange}
          value={this.props.checkboxMedia}
        />
      </FormControl>
    );
  }
}

export default withStyles(styles)(SearchMedia);

