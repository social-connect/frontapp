import React, { Component } from 'react';

import { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
import { lightGreen } from 'material-ui/colors';

import { getContributors } from '../../../services/api';
import ResetFormButton from '../resetFormButton/';


const styles = theme => ({
  control: {
    minWidth: "32%",
    flex: 'inline-flex',
    flexDirection: 'row',  
  },
    label: {
    color: lightGreen[600],
  },
  select:{
    width: '100%'
  }
})

class SearchSourceur extends Component {
  state= {
    contributors: []
  }

  clearFilterSourceur = () => {
    this.props.clearFilterSourceur()
  }

componentDidMount(){
  getContributors()
    .then(response => {
      return response.json()
    })
    .then(jsonResponse => {
      this.setState({
        contributors: jsonResponse.results
      })
    })
}


  render(){
    const classes = this.props.classes;
    return(
      <FormControl className={classes.control} >
        <InputLabel
          htmlFor="rechercheSourceur"
          className={classes.label}
        >Recherche par contributeur
        </InputLabel>
        <Select
          className={classes.select} 
          value={this.props.selectSourceur}
          onChange={this.props.handleFilterSourceurChange}
        > {this.state.contributors.map((sourceur, i) =>
          <MenuItem key={"sourceur-" + i} value={sourceur['spider_name']}>{sourceur['name']}</MenuItem>
          )}
        </Select>
        {this.props.selectSourceur && <ResetFormButton clearInputValue={this.clearFilterSourceur}/>}
      </FormControl>
    )
  }
}
export default withStyles(styles)(SearchSourceur);
