import React, { Component } from 'react';

import SearchByKeyword from './searchByKeyword/';
import SearchLibre from './searchLibre/';
import SearchRegion from './searchRegion/';
import SearchDepartement from './searchDepartement/';
import SearchSourceur from './searchSourceur/';
import SearchMedia from './SearchMedia/';

import { withStyles } from 'material-ui/styles';
import { blueGrey } from 'material-ui/colors';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  searchForm: {
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '30px 120px',
    color: blueGrey[600],
  },
  searchFormControl: {
    width: '100%'
  }
});


class SearchForm extends Component {

  handleFilterTextChange = (e) => {
    this.props.onFilterTextChange(e.target.value);
  }
  clearFilterText = () => {
    this.props.onFilterTextClear()
  }

  handleFilterMediaChange = (e) => {
    this.props.onCheckboxMediaChange(e.target.value);
  }

  handleFilterRegionChange = (e) => {
    this.props.onFilterRegionChange(e.target.value);
  }
  clearFilterRegion = () => {
    this.props.onFilterRegionClear()
  }

  handleFilterDepartementChange = (e) => {
    this.props.onFilterDepartementChange(e.target.value);
  }
  clearFilterDepartement = () => {
    this.props.onFilterDepartementClear()
  }

  handleFilterSourceurChange = (e) => {
    this.props.onFilterSourceurChange(e.target.value);
  }
  clearFilterSourceur = () => {
    this.props.onFilterSourceurClear()
  }

  handleFilterKeywordChange = (keyword) => {
    this.props.onFilterKeywordChange(keyword);
  }
  clearFilterKeyword = () => {
    this.props.onFilterKeywordClear()
  }


  render() {

    const classes = this.props.classes;

    return (
      <div className={classes.searchForm}>
        <Typography type="display1">Recherchez des projets liés à l'innovation sociale</Typography>
        <SearchLibre
          handleFilterTextChange={this.handleFilterTextChange}
          clearFilterText={this.clearFilterText}
          filterText={this.props.filterText}
        />
        <SearchByKeyword
          handleFilterKeywordChange={this.handleFilterKeywordChange}
          clearFilterKeyword={this.clearFilterKeyword}
          keywordInput={this.props.keywordInput}
        />
        <div className={classes.searchFormControl}>
          <SearchRegion
            handleFilterRegionChange={this.handleFilterRegionChange}
            clearFilterRegion={this.clearFilterRegion}
            selectRegion={this.props.selectRegion}
          />
          <SearchDepartement
            handleFilterDepartementChange={this.handleFilterDepartementChange}
            clearFilterDepartement={this.clearFilterDepartement}
            selectDepartement={this.props.selectDepartement}
          />
          <SearchSourceur
            handleFilterSourceurChange={this.handleFilterSourceurChange}
            clearFilterSourceur={this.clearFilterSourceur}
            selectSourceur={this.props.selectSourceur}
          />
          <SearchMedia
            handleFilterMediaChange={this.handleFilterMediaChange}
            checkboxMedia={this.props.checkboxMedia}
          />
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(SearchForm);
