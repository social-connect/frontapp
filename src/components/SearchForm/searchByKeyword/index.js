import React, { Component } from 'react';

import { FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import { InputLabel } from 'material-ui/Input';
import { lightGreen, blueGrey } from 'material-ui/colors';

import { Async } from 'react-select';
import 'react-select/dist/react-select.css';

import { getKeywords } from '../../../services/api';

import './styles.css';

const styles = theme => ({
  control:{
    flex: 'inline-flex',
    flexDirection: 'row',
  },
  input: {
    color: blueGrey[600]
  },
  label: {
    color: lightGreen[600]
  }
});


class SearchByKeyword extends Component {

  state = {
    value: [],
  };


  getOptions(input) {
    console.log(input)
    return getKeywords(input)
      .then(response => {
        return response.json()
      })
      .then(jsonResponse => {
        return { options : jsonResponse.results };
      })
  }

  handleSelectChange = (value) => {
    this.setState({ value });
    console.log(value)
    this.props.handleFilterKeywordChange(value);
  }


  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className="SearchByKeywordContainer">
      <FormControl className="searchForm-control SearchByKeyword">
        <div className="partForm">
          <InputLabel
            className={classes.label}
            htmlFor="keywordsSearch"
            shrink={(value.length > 0) ? true : false}
            >Recherche à partir des mots clés existants
          </InputLabel>
          <Async
            closeOnSelect
            multi
            stayOpen
            onChange={this.handleSelectChange}
            placeholder=""
            removeSelected
            value={value}
            valueKey={'id'}
            labelKey={'label'}
            autoload
            loadOptions={ this.getOptions }
          />
        </div>
    </FormControl>
    </div>
    );
  }
}

export default withStyles(styles)(SearchByKeyword);
