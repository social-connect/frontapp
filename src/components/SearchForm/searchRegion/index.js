import React, { Component } from 'react';

import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
import { lightGreen } from 'material-ui/colors';

import { regions } from '../../../assets/config.json';
import ResetFormButton from '../resetFormButton/';

const styles = theme => ({
  control: {
    minWidth: "32%",   
    flex: 'inline-flex',
    flexDirection: 'row',   
  },
    label: {
      color: lightGreen[600]
  },
  select:{
    width: '100%'
  }
})

class SearchRegion extends Component {

  clearFilterRegion = () => {
    this.props.clearFilterRegion()
  }

  render(){
    const classes = this.props.classes;
    
    return(
      <FormControl className={classes.control} >
        <InputLabel 
          className={classes.label} 
          htmlFor="rechercheRegion"
          >
          Recherche par région
        </InputLabel>
        <Select
          className={classes.select}
          multiple
          value={this.props.selectRegion}
          onChange={this.props.handleFilterRegionChange}
          input={<Input id="regMultiple" />}
        > {regions.map((region, i) =>
          <MenuItem key={"region"+i} value={region}>{region}</MenuItem>
          )}
        </Select>
        {this.props.selectRegion.length > 0  && <ResetFormButton clearInputValue={this.clearFilterRegion}/>}
      </FormControl>
    )
  }
}

export default withStyles(styles)(SearchRegion);

