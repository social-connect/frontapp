import React, { Component } from 'react';

import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import { lightGreen, blueGrey } from 'material-ui/colors';

import ResetFormButton from '../resetFormButton/';


const styles = theme => ({
  control:{
    flex: 'inline-flex',
    flexDirection: 'row',
  },
  input: {
    color: blueGrey[600]
  },
  label: {
    color: lightGreen[600]
  }
})

class SearchLibre extends Component{

  clearFilterText = () => {
    this.props.clearFilterText()
  }

  render(){
    const classes = this.props.classes;

    return(
      <FormControl className={classes.control} fullWidth>
        <InputLabel
          className={classes.label}
          htmlFor="rechercheLibre"
          >Recherche libre
        </InputLabel>
        <Input
          placeholder="saisir du texte (pour une recherche sur plusieurs expressions, séparez-les d'une virgule)"
          className={classes.input}
          value={this.props.filterText}
          onChange={this.props.handleFilterTextChange}
          fullWidth
        />
        {this.props.filterText && <ResetFormButton clearInputValue={this.clearFilterText}/>}
      </FormControl>
    )
  }
}
export default withStyles(styles)(SearchLibre);

