import React, { Component } from 'react';

import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
import { lightGreen } from 'material-ui/colors';

import { departements } from '../../../assets/config.json';
import ResetFormButton from '../resetFormButton/';

const styles = theme => ({
  control: {
    minWidth: "32%", 
    marginLeft: '2%',
    marginRight: '2%',
    flex: 'inline-flex',
    flexDirection: 'row',   
  },
    label: {
    color: lightGreen[600]
  },
  select:{
    width: '100%'
  }
    
})

class SearchDepartement extends Component {

  clearFilterDepartement = () => {
    this.props.clearFilterDepartement()
  }

  render(){
    const classes = this.props.classes;
    
    return(
      <FormControl className={classes.control} >
        <InputLabel 
          className={classes.label} 
          htmlFor="rechercheRegion"
          >
          Recherche par département
          </InputLabel>
        <Select
          className={classes.select} 
          multiple
          value={this.props.selectDepartement}
          onChange={this.props.handleFilterDepartementChange}
          input={<Input id="deptMultiple" />}
        > {departements.map((departement, i) =>
          <MenuItem key={"dept"+i} value={departement}>{departement}</MenuItem>
          )}
        </Select>
        {this.props.selectDepartement.length > 0 && <ResetFormButton clearInputValue={this.clearFilterDepartement}/>}
      </FormControl>      
    )
  }
}
export default withStyles(styles)(SearchDepartement);