import React, { Component } from 'react';

import ProjectsTableBody from './ProjectsTableBody'
import ProjectsTableHead from './ProjectsTableHead'
import Table, { TableBody, TableRow, TableFooter, TablePagination } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';

import { compareValues } from './ProjectsViewTable.helper';

const columnData = [
  { id: 'title', label: 'Titre de l\'article', numeric: false, sortable: true },
  { id: 'area', label: 'Localisation', numeric: false, sortable: false },
  { id: 'spider_name', label: 'Contributeur', numeric: false, sortable: true }
];

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  ProjectListItem: {
    justifyContent: 'flex-start'
  }
});

class ProjectViewTable extends Component {

  state = {
    page: 0,
    rowsPerPage: 5,
    selectedProject: {},
    orderBy: 'title',
    order: 'asc',
    projectsSorted: []
  };

  componentDidMount() {
    this.setState({
      projectsSorted: this.props.projects,
    }, () => {
      this.updateSort();
    })
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.projects) !== JSON.stringify(nextProps.projects)) {
      this.setState({
        projectsSorted: nextProps.projects,
      }, () => {
        this.updateSort();
      })
    }
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  updateSort = (orderBy = this.state.orderBy) => {
    let order = 'asc';
    if (this.state.orderBy === orderBy && this.state.order === 'asc') {
      order = 'desc';
    }
    this.setState({
      projectsSorted: this.state.projectsSorted.sort(compareValues(orderBy, order)),
      order,
      orderBy,
    })
  }

  render() {
    const { rowsPerPage, page, projectsSorted, order, orderBy } = this.state;
    const { classes, projects } = this.props;

    return(
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <ProjectsTableHead
              updateSort={this.updateSort}
              order={order}
              orderBy={orderBy}
              columnData={columnData}
            />
            <TableBody>
            {
              projectsSorted.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((project, i) => {
              return (
                <ProjectsTableBody
                  key={'projectRow'+i}
                  project={project}
                  orderBy={orderBy}
                  order={order}
                />
              );
            })}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  count={projects.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
    );
  }
}

export default withStyles(styles)(ProjectViewTable);

