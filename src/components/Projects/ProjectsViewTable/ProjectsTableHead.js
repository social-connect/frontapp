import React from 'react';

import { TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui/Table';
import { SwapVert } from 'material-ui-icons';
import { blueGrey } from 'material-ui/colors';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  label: {
    color: blueGrey[600],
    fontSize: '1.7em'
  }, 
  icone: {
    width:'1em'
  }
})

class ProjectsTableHead extends React.Component {

  handleUpdateSort = (id) => {
    return (event) => {
      this.props.updateSort(id);
    }
  }

  render() {

    const { classes, order, orderBy, columnData } = this.props;
    return (
      <TableHead>
        <TableRow>
          {
            columnData.map(column => (
              <TableCell
                key={column.id}
              >
                <TableSortLabel
                  className={classes.label}
                  active={column.sortable ? orderBy === column.id : false}
                  direction={column.sortable ? order : 'desc'}
                  onClick={column.sortable && this.handleUpdateSort(column.id)}
                > 
                  { column.sortable && <SwapVert className={classes.icone}/> }
                  {column.label}
                </TableSortLabel>
              </TableCell>
            ))
          }
        </TableRow>
      </TableHead>
    );
  }
}
export default withStyles(styles)(ProjectsTableHead);
