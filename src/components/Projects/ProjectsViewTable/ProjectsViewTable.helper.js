export const compareValues = function (orderBy, order) {
  return (a, b) => {
    if (!a.hasOwnProperty(orderBy) || !b.hasOwnProperty(orderBy)) {
      return 0;
    }
    const varA = (typeof a[orderBy] === 'string') ? a[orderBy].toUpperCase() : a[orderBy];
    const varB = (typeof b[orderBy] === 'string') ? b[orderBy].toUpperCase() : b[orderBy];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
};
