import React, { Component } from 'react';

import { TableRow, TableCell } from 'material-ui/Table';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import { sourceurs } from '../../../assets/config.json';


const styles = theme => ({ button: { fontSize: '1em' } });

class ProjectsTableBody extends Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
      page: 0,
      rowsPerPage: 5,
      selectedProject: {},
    };
  }

  renderProjectTitleTable (link, title) {
    let sourcUrl;
    if (link && link.length > 0) {
      sourcUrl = new URL(link);
      return <Button className={this.props.classes.button} href={sourcUrl} target="_blank">{title}</Button>;
    }
    return null;
  }

  render_area (area) {
    let areas = [];
    if (area.regions && area.regions.length > 0) {
      areas = area.regions;
    }
    if (area.departements && area.departements.length > 0) {
      areas = area.departements;
    }
    if (area.communautes_communes && area.communautes_communes.length > 0) {
      areas = area.communautes_communes;
    }
    if (area.communes && area.communes.length > 0) {
      areas = area.communes;
    }
    if (areas.length > 0) {
      return areas.join(', ');
    }
    return null;
  }

  render () {
    const project = this.props.project;

    return (
      <TableRow
        xs={12}
        sm={6}
        lg={4}
        xl={3}
        className="project-list"
      >
        <TableCell>{this.renderProjectTitleTable(project.link, project.title)}</TableCell>
        <TableCell>{this.render_area(project.area) || this.na_message}</TableCell>
        <TableCell>{(sourceurs.map((sourceur, i) => sourceur.key === project.spider_name && sourceur.label)) || this.na_message }</TableCell>
      </TableRow>
    );
  }
}

export default withStyles(styles)(ProjectsTableBody);

