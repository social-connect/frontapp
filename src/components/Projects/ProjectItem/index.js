import React, { Component } from 'react';

import Card, { CardActions, CardMedia, CardContent } from 'material-ui/Card';
import Collapse from 'material-ui/transitions/Collapse';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';

import { blueGrey, lightGreen } from 'material-ui/colors';

import classnames from 'classnames';

import './style.css';
import pictoLocalisation from '../../../assets/localisation.png';
import {sourceurs} from '../../../assets/config.json';


const styles = theme => ({
  header: {
    fontSize: '1.125em',
    fontWeight: 'bold',
    color: blueGrey[600]
  },
  media: {
    height: 140,
    backgroundSize: "contain",
  },
  abstract: {
    marginBottom: 5
  },
  contentSecond: {
    height: 'maxContent'
  },
  MoreButtonContainer: {
    textAlign: 'center',
    display: 'block',
    height: '20px',
    alignItem: 'top'
  },
  expand: {
    display: 'inline-bloc',
    mariginRight: 'auto',
    mariginLeft: 'auto',
    backgroundColor:  blueGrey[400],
    color: 'white',
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  collapse: {
    fontSize: '0.85em',
  },
  picto: {
    width: 16
  },
  titleButton: {
    color: blueGrey[600],
    fontSize: '1em',
    textAlign: 'left'
  },
  projectLink: {
    color: lightGreen[400],
    fontSize: '0.95em'
  },
});


class ProjectItem extends Component {

  constructor(props) {
    super(props);

    this.na_message = <em> Non renseigné </em>

    this.state = {
      expanded: false,
      isToggleOn: false,
    };

    // This binding is necessary to make `this` work in the callback
    // this.handleCardClick = this.handleCardClick.bind(this);
  }

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };
  handleCardClick = (e) => {
    this.props.onSelectProject(this.props.project);
  }

  renderProjectTitle(title) {
    return <Button className={this.props.classes.titleButton} onClick={this.handleExpandClick}>{title}</Button>
  }

  render_abstract(abstract) {
    if (abstract) {
      return abstract.substring(0,150);
    }
    return null
  }

  render_area(area) {
    let areas = []
    if (area.regions && area.regions.length > 0) {
      areas = area.regions
    }
    if (area.departements && area.departements.length > 0) {
      areas = area.departements
    }
    if (area.communautes_communes && area.communautes_communes.length > 0) {
      areas = area.communautes_communes
    }
    if (area.communes && area.communes.length > 0) {
      areas = area.communes
    }
    if (areas.length > 0) {
      return areas.join(', ')
    }
    return null
  }

  render_keywords(keywords) {
    if (keywords) {
      return (
        Array.prototype.slice.call(keywords).map((keyword, i) =>
          (keywords === -1) ? null :
          <li className="keywordItem" key={"keyword"+i}>
            <Chip
              className='chip'
              label={keyword}
              onClick={this.handleChipClick}
              >
            </Chip>
          </li>
        )
      )
    }
    return null
  }

  render_projectLink(link) {
    let sourcUrl;
    const classes = this.props.classes;
    if (link && link.length > 0) {
      sourcUrl = new URL(link);
      return (
        <Button component='a' dense className={classes.projectLink} href={sourcUrl} target="_blank">
          Voir sur le site
        </Button>
      )
    }
    return null
  }

  handleChipClick() {
  }

  renderContact(contact) {
    let regExp = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    if (contact.match(regExp)) {
      let extractedEmail = regExp.exec(contact);
      let href = "mailto:"+extractedEmail[0]
      return  <a href={href} target="_top">{extractedEmail[0]}</a>
    }
    return null
  }

  renderProjectVideo(videos) {
    if (videos) {
      return (
        Array.prototype.slice.call(videos).map((video, i) =>
          (videos === -1) ? null :
          <li className={this.props.videoItem} key={"video"+i} style={{listStyleType:'none'}}>
            <a className={this.props.linkVideo} href={video} target="_blank" alt="video" >{videos.length>1 ? 'vidéo'+i+1 : 'vidéo' }</a>
          </li>
        )
      )
    }
    return null
  }

  render() {
    const { project, isSelected } = this.props;
    const classes = this.props.classes;

    return (
        <Card className="projectItem-container"
        elevation={ (isSelected && 10) || 2}
        onClick={this.handleCardClick}>
        <CardContent>
          <Typography type="headline" variant="display4"
            className={classes.header}>
            {this.renderProjectTitle(project.title)}
          </Typography>
        </CardContent>
        { project.img &&
          <CardMedia
            image={project.img}
            title={project.title}
            className={classes.media}
          />
        }
        <CardContent className={classes.contentSecond}>
          <Typography>{ this.render_abstract(project.abstract) }…</Typography>
          <Typography>
            <img className={classes.picto} src={pictoLocalisation}
              alt="localisation"
            /> { this.render_area(project.area) || this.na_message }
          </Typography>
          <ul className="projectKeyWords">
            { (this.state.expanded && this.render_keywords(project.keywords))
              || this.render_keywords(project.keywords).slice(0, 3) }
          </ul>
        </CardContent>
        <CardActions className={classes.MoreButtonContainer}>
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded,
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse
          in={this.state.expanded}
          timeout="auto"
          unmountOnExit
          className={classes.collapse}
        >
          <CardContent className="projectMore-infos-text">

            <p>{this.render_projectLink(project.link)}</p>
            <p className="projectSourceur">Source : { (sourceurs.map((sourceur, i)=> sourceur.key === project.spider_name && sourceur['label'])) || this.na_message }</p>
            <p className="projectContact">Contact : { this.renderContact(project.contact) || this.na_message }</p>
            <p className="projectHolder">Porteur du projet : { project.project_holder || this.na_message }</p>
            {( project.video && project.video.length > 0) && (
              <div className="projectVideos"><p>Videos : </p><ul className="listVideo">{ this.renderProjectVideo(project.video)}</ul></div>
            )}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}


export default withStyles(styles)(ProjectItem);
