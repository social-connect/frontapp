import React, { Component } from 'react';

import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import ViewModule from 'material-ui-icons/ViewModule';
import List from 'material-ui-icons/List';
import { withStyles } from 'material-ui/styles';
import { blueGrey, lightGreen } from 'material-ui/colors';

import ProjectsViewTable from './ProjectsViewTable/'
import ProjectsViewGrid from './ProjectsViewGrid/'

import MapComponent from './Map/';


const styles = theme => ({
  container: {
    position: 'relative',
    margin: '20px',
    maxHeight:'100%',
    display: 'flex',
    flexDirection: 'column',
  },
  infoResultBar: {
    display: 'bloc',
    width: '100%',
    height:'maxContent',
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  infosGen: {
    color: blueGrey[400],
    display: 'inline-block',
    width: 'maxContent'
  },
  viewButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    display: 'inline-block',
  },
  resultNav: {
    backgroundColor: blueGrey[100],
    marginBottom: 24
  },
  resultInfos: {
    flexGrow: 1,
  },
  containerResult: {
    display: 'flex',
    flexDirection: 'row',
  }
})

class Projects extends Component {

  constructor(props) {
    super(props);
    this.numberProjectsDisplayed = 24
    this.gridView = true

    this.state = {
      selectedProject: {},
      gridView: true
    }
  }

  handleSelectedProjectChange = (selectedProject) => {
    this.setState({
      selectedProject: selectedProject
    });
  }

  handleChangeView = () => {
    this.setState((prevState) => ({
      gridView: !prevState.gridView
    }));
  }

  render() {
    const { hasFilters, newView } = this.props;
    const { classes, projects, projectsCount } = this.props;

    return (
      <div className={classes.container}>
        <div className={classes.infoResultBar}>
        {
        (!hasFilters &&
          <h2 className={classes.infosGen}>Derniers projets</h2>
            ) || (
              <div>
                <h2 className={classes.infosGen}>Résultats {newView && '(ordre aléatoire)'} </h2>
                <Grid container className={classes.resultNav}>
                  <Grid item className={classes.resultInfos}>
                    {(
                      projectsCount > 0 &&
                      <Typography>{projectsCount} projets ({(projectsCount < this.numberProjectsDisplayed && 'tous') || this.numberProjectsDisplayed } affichés)</Typography>
                    ) ||
                      <Typography>Pas de projets avec les filtres sélectionnés</Typography>
                    }
                  </Grid>
                  <Grid item className="ResultsActions">
                  </Grid>
                </Grid>
              </div>
              )
          }
          <div className={classes.viewButton}>
            <Button
              onClick={this.handleChangeView}
              raised style={{ backgroundColor: lightGreen[600], color: '#FEFEFE' }}
            > {
                this.state.gridView
                  ? <List />
                  : <ViewModule />
              }
            </Button>
          </div>
          <Grid className={classes.containerResult} container spacing={24}>
            <Grid item xs={12} sm={8} lg={8}>
              {
                (!this.state.gridView &&
                  <ProjectsViewTable
                    projects={projects}
                  />
                ) || (
                  <ProjectsViewGrid
                    projects={projects}
                    numberProjectsDisplayed={this.numberProjectsDisplayed}
                    onSelectProject={this.handleSelectedProjectChange}
                    selectedProject={this.state.selectedProject}
                  />
                )
              }

            </Grid>
            <Grid item hidden={{ smDown: true }} xs={12} sm={4} lg={4}>
              <MapComponent
                projects={projects.slice(0, this.numberProjectsDisplayed)}
                selectedProject={this.state.selectedProject}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}


export default withStyles(styles)(Projects);
