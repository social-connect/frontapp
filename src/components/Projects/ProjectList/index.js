import React, { Component } from 'react';

import { withStyles } from 'material-ui/styles';

import ProjectsViewTable from '../ProjectsViewTable/';
import ProjectsViewGrid from '../ProjectsViewGrid/';


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  ProjectListItem: { justifyContent: 'flex-start' },
});

class ProjectList extends Component {
  render () {
    const { newView, projects, project, selectedProject, onSelectProject, numberProjectsDisplayed } = this.props;

    return (
      !newView
        ?
          <ProjectsViewTable
            projects={projects}
            onSelectProject={onSelectProject}
          />
        :
          <ProjectsViewGrid
            projects={projects}
            onSelectProject={onSelectProject}
            numberProjectsDisplayed={numberProjectsDisplayed}
            isSelected={project === selectedProject && selectedProject}
          />
    );
  }
}

export default withStyles(styles)(ProjectList);

