import React, { Component } from 'react';
import L from 'leaflet';

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';


const styles = {
  root: {
    overflow: 'hidden',
    maxHeight: 900,
  },
  buttonOpen: {
    position: 'fixed',
    backgroundColor: '#02419A',
    color: '#FFF',
  },
};


class MapComponent extends Component {
  constructor (props) {
    super(props);
    this.position = [46.506, 2.186];
    this.map = {};
    this.markersLayer = L.featureGroup();
    this.markersList = [];
  }

  componentDidMount () {
    this.map = L.map('projects-map').setView(this.position, 5);

    L.tileLayer('//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', { attribution: '<a href="http://osm.org/copyright">OpenStreetMap</a> contributors' }).addTo(this.map);

    this.markersLayer.addTo(this.map);

    !!this.props.projects && this.renderMarkers(this.props.projects);
  }

  componentWillReceiveProps (nextProps) {
    // JSON.stringify(JSON.parse(montableaudavant)) !== JSON.stringify(JSON.parse(montableauactuel))
    if (nextProps.projects !== this.props.projects) {
      this.renderMarkers(nextProps.projects);
    }
    if ((nextProps.selectedProject !== this.props.selectedProject) && (nextProps.selectedProject.coordinates)) {
      this.openMarkerPopup(nextProps.selectedProject);
    }
  }

  renderMarkers (projects) {
    this.markersList = [];
    this.markersLayer.clearLayers();
    const lats = [];
    const longs = [];

    projects.map((project, i) => {
      if (project.coordinates !== null && project.coordinates[0]) {
        lats.push(project.coordinates[1]);
        longs.push(project.coordinates[0]);
        const marker = L.marker([project.coordinates[1], project.coordinates[0]])
          .addTo(this.markersLayer)
          .bindPopup(project.title);
        this.markersList[project.idx] = marker;
        return marker;
      }
      return null;
    });

    if (lats.length > 0 && longs.length > 0) {
      this.map.fitBounds([
        [Math.min.apply(null, lats), Math.min.apply(null, longs)],
        [Math.max.apply(null, lats), Math.max.apply(null, longs)],
      ]);
    }
  }

  openMarkerPopup (selectedProject) {
    if (selectedProject.coordinates !== null && selectedProject.coordinates[0]) {
      const marker = this.markersList[selectedProject.idx];
      marker.openPopup();
    }
  }

  render () {
    const { classes } = this.props;

    return (
      <Paper className={classes.root} elevation={4}>
        <div id="projects-map" />
      </Paper>
    );
  }
}

export default withStyles(styles)(MapComponent);
