import React, { Component } from 'react';

import ProjectItem from '../ProjectItem/';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({ ProjectListItem: { justifyContent: 'flex-start' } });

class ProjectsViewGrid extends Component {
  render () {
    const { classes, projects, selectedProject, numberProjectsDisplayed } = this.props;

    return (
      <Grid container spacing={24}>
        {
        projects.slice(0, numberProjectsDisplayed).map((project, i) => (
          <Grid
            key={`project${i}`}
            item
            xs={12}
            sm={6}
            lg={4}
            xl={3}
            className={classes.ProjectListItem}
          >
            <ProjectItem
              key={this.uniqueID}
              project={project}
              onSelectProject={this.props.onSelectProject}
              isSelected={project === selectedProject && selectedProject}
            />
          </Grid>
          ))
      }
      </Grid>
    );
  }
}
export default withStyles(styles)(ProjectsViewGrid);

