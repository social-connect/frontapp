import React, { Component } from 'react';
import { debounce } from 'throttle-debounce';

import SearchForm from './SearchForm/';
import Projects from './Projects/';

import { getProjects } from '../services/api';
import topprojects from '../data/topprojects.json';

// import {
//   filterAbstract,
//   filterTitle,
//   filterRegion,
//   filterDepartement,
//   filterSourceur,
//   projectHasKeyword,
//   filterMedia
// } from '../services/filters';


class SearchProjectApp extends Component {

  constructor(props) {
    super(props)
    this.state = {
      filterText: '',
      selectRegion: [],
      selectDepartement: [],
      keywordInput: [],
      suggestionsSelected: [],
      selectSourceur: '',
      checkboxMedia: false,
      hasFilters: false,
      projects: [],
      projectsCount: 0,
    };

    this.debounceUpdateProjectFiltered = debounce(200, this.updateProjectFiltered)
  }

  updateProjectFiltered() {
    let keywordListSring = this.state.keywordInput.map((keyword, i) => keyword.value).join(',');
    getProjects(this.state.filterText, keywordListSring, this.state.selectSourceur, this.state.selectRegion, this.state.selectDepartement, this.state.checkboxMedia)
      .then(response => {
        return response.json()
      })
      .then(jsonResponse => {
        this.setState({
          projects: jsonResponse.results,
          projectsCount: jsonResponse.count
        })
      });
  }

  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText,
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  cleanFilterTextChange = (filterText) => {
    this.setState({
      filterText: '',
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  handleCheckboxMediaChange = (checkboxMedia) => {
    this.setState({
      checkboxMedia: !this.state.checkboxMedia,
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }

  handleFilterKeywordChange = (keywordInput) => {
    this.setState({
      keywordInput: keywordInput,
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }

  clearFilterKeyword = () => {
    this.setState({
      keywordInput: '',
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  handleListKeywordsUpdate = (keywordInput) => {
    this.setState({
      suggestionsSelected: [keywordInput],
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  handleFilterRegionChange = (selectRegion) => {
    this.setState({
      selectRegion: selectRegion,
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }
  cleanFilterRegionChange = (selectRegion) => {
    this.setState({
      selectRegion: [],
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  handleFilterDepartementChange = (selectDepartement) => {
    this.setState({
      selectDepartement: selectDepartement,
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }
  cleanFilterDepartementChange = (selectDepartement) => {
    this.setState({
      selectDepartement: [],
      hasFilters: this.hasFilters(),
    })
    this.debounceUpdateProjectFiltered();
  }

  handleFilterSourceurChange = (selectSourceur) => {
    this.setState({
      selectSourceur: selectSourceur,
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }
  cleanFilterSourceurChange = (selectSourceur) => {
    this.setState({
      selectSourceur: '',
      hasFilters: this.hasFilters(),
    });
    this.debounceUpdateProjectFiltered();
  }

  hasFilters = () => {
    return (
      this.state.filterText !== '' ||
      this.state.keywordInput.length > 0 ||
      this.state.selectRegion.length > 0 ||
      this.state.selectDepartement.length > 0 ||
      this.state.selectSourceur !== '' ||
      this.state.checkboxMedia
    )
  }

  componentDidMount() {
    getProjects()
      .then(response => {
        return response.json()
      })
      .then(jsonResponse => {
        this.setState({
          projects: jsonResponse.results
        })
      });
  }


  render() {

    let listedprojects = [];
    let hasFilters = this.hasFilters();
    if (hasFilters) {
      listedprojects = this.state.projects;
    } else {
      listedprojects = topprojects;
    }

    return (
      <div className="Search-app">
        <SearchForm
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
          onFilterTextClear={this.cleanFilterTextChange}

          onCheckboxMediaChange={this.handleCheckboxMediaChange}

          keywordInput={this.state.keywordInput}
          onFilterKeywordChange={this.handleFilterKeywordChange}
          clearFilterKeyword={this.clearFilterKeyword}
          suggestionsSelected={this.state.suggestionsSelected}

          selectRegion={this.state.selectRegion}
          onFilterRegionChange={this.handleFilterRegionChange}
          onFilterRegionClear={this.cleanFilterRegionChange}

          selectDepartement={this.state.selectDepartement}
          onFilterDepartementChange={this.handleFilterDepartementChange}
          onFilterDepartementClear={this.cleanFilterDepartementChange}

          selectSourceur={this.state.selectSourceur}
          onFilterSourceurChange={this.handleFilterSourceurChange}
          onFilterSourceurClear={this.cleanFilterSourceurChange}
        />
        <Projects
          projects={listedprojects}
          projectsCount={this.state.projectsCount}
          hasFilters={this.hasFilters}
        />
      </div>
    );
  }
}

export default SearchProjectApp;
