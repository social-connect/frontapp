import React, { Component } from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';

import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import { lightGreen, blueGrey } from 'material-ui/colors';

import ScrollRestoration, { RouteWithSubRoutes, routes } from './routes';
import './App.css';
import logoEig from './assets/logo-entrepreneure-ig.png';
import logoCGET from './assets/cget-logo.png';
import logoFonda from './assets/fonda-logo.png';

import Loader from './components/Loader/';


const styles = {
  ButtonHeader: {
    color: blueGrey[300]
  },
  appTitle : {
    color: lightGreen[600],
  },
  appSlogan: {
    color: blueGrey[500],
  },
  tabInfos: {
    margin: '15px auto 0 auto',
  },
  selected: {
    backgroundColor: blueGrey[50]
  }
}

class App extends Component {
  
  state = { 
    loading: true
  };

  componentDidMount() {
    setTimeout(() => 
      this.setState({
        loading: false
      }), 1500)
  };

  render() {

    const { classes } = this.props;
    const { loading } = this.state;

    return (
      <Router>
        <ScrollRestoration>
          <div className="App">
            <Grid container component="header" className="App-header">
              <div className="headerContent">
                <div className="logoProject"/>
                <div className="headerContentText">
                  <Grid item xs={12} className="headerTitles">
                    <Typography type="title" className={classes.appTitle}>Le carrefour des innovations sociales</Typography>
                    <Typography type="subheading" className={classes.appSlogan}>Mettre en commun des solutions au service des personnes et des territoires</Typography>
                  </Grid>
                  <Grid container className={classes.tabInfos} >
                    <NavLink to='/' exact activeClassName={classes.selected} >
                      <Button component='span' className={classes.ButtonHeader}>Accueil</Button>
                    </NavLink>
                    <NavLink to='/rechercher' activeClassName={classes.selected} >
                      <Button component='span' className={classes.ButtonHeader}>Rechercher des projets</Button>
                    </NavLink>
                    <NavLink to='/apropos' activeClassName={classes.selected} >
                      <Button component='span' className={classes.ButtonHeader}>À propos</Button>
                    </NavLink>
                    <NavLink to='/contributeurs' activeClassName={classes.selected} >
                      <Button component='span' className={classes.ButtonHeader}>Contributeurs</Button>
                    </NavLink>
                  </Grid>
                </div>
              </div>
            </Grid>

            {loading ? <Loader /> : routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
            <div className="App-footer">
              <div className="laureat">
                <p>un projet lauréat</p>
                <img src={logoEig} alt="Entrepreneure d'intérêt général" />
              </div>
              <div className="partenaires">
                <p>animé par</p>
                <img src={logoCGET} alt="Commissariat Général à l’Egalité des Territoires" />
                <img src={logoFonda} alt="La fonda : fabrique associative" />
              </div>
            </div>
          </div>
          </ScrollRestoration>
        </Router>
    );
  }
}


export default withStyles(styles)(App);
