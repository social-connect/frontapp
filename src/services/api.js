import 'whatwg-fetch';

const baseUrl =  'https://social-connect-cget.herokuapp.com';

const init = {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  credentials: 'omit',
  mode: 'cors',
};

export const getProjects = (title = '', keywords = '', contributor = '', areaReg = '', areaDept = '', hasMedia = '') => {
  let projectList = '/api/projects/?';
  if (title !== '') {
    projectList = `${projectList}search=${title}`;
  }
  if (keywords !== '') {
    projectList = `${projectList}&keywords__value__in=${keywords}`;
  }
  if (contributor !== '') {
    projectList = `${projectList}&contributor__spider_name=${contributor}`;
  }
  if (areaReg !== '') {
    projectList = `${projectList}&region=${areaReg}`;
  }
  if (areaDept !== '') {
    projectList = `${projectList}&department=${areaDept}`;
  }
  if (hasMedia == true) {
    projectList = `${projectList}&has_video=${hasMedia}`;
  }
  projectList += '&page_size=100';
  const projectsUrl = baseUrl + projectList;
  const projectsRequest = new Request(projectsUrl, init);
  return fetch(projectsRequest);
};

export const getKeywords = (label = '') => {
  let keywordsList = '/api/keywords/';
  if (label !== '') {
    keywordsList = `${keywordsList}?label__icontains=${label}`;
  }
  const keywordsUrl = baseUrl + keywordsList;
  const keywordsRequest = new Request(keywordsUrl, init);
  return fetch(keywordsRequest);
};

export const getContributors = () => {
  const contributorList = '/api/contributors/';
  const contributorUrl = baseUrl + contributorList;
  const contributorsRequest = new Request(contributorUrl, init);
  return fetch(contributorsRequest);
};
