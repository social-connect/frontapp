import { noDiacritics } from './utils';

export const filterKeyword = (project, keywordInput) => {
  let hasKeyword = false;

  if (project.keywords && project.keywords.length > 0) {
    let keywords = project.keywords;
    const keywordInputLowerCase = noDiacritics(keywordInput.toLocaleLowerCase());
    if (!(project.keywords instanceof Array)) {
      keywords = [project.keywords];
    }

    keywords.forEach(keyword => {
      if (noDiacritics(keyword).toLocaleLowerCase().indexOf(keywordInputLowerCase) > -1) {
        hasKeyword = true;
        return hasKeyword;
      }
    });
  }
  return hasKeyword;
};

/**
 * Analyse a project and check if it has
 * one of keywordsInput inside keywords property
 *
 * @param {Object} project Project to be analyzed
 * @param {Array<{id,label}>} keywordsInput array of {id, label} = keyword
 *
 * @return {boolean} true or false, if the project has one of keywordsInput or not
 */
export const projectHasKeyword = (project, keywordsInput) => {
  if (!project.keywords || project.keywords.length < 1) return false;
  if (Array.isArray(keywordsInput) && keywordsInput.length < 1) return true;

  let result = false;
  const projectKeywords = project.keywords instanceof Array
    ? project.keywords
    : [project.keywords];

  const projectKeywordsLowerCase = projectKeywords.map(pk => noDiacritics(pk).toLocaleLowerCase());
  const keywordsInputLowerCase = keywordsInput.map(ki => noDiacritics(ki.label.toLocaleLowerCase()));

  projectKeywordsLowerCase.forEach(pk => {
    keywordsInputLowerCase.forEach(ki => {
      if (pk.indexOf(ki) > -1) {
        result = true;
      }
    });
  });
  return result;
};

export const filterTitle = (project, filterText) => {
  let hasFilterText = false;
  const listFilterText = filterText.split(',');
  listFilterText.forEach(filterText => {
    if (noDiacritics(project.title.toLocaleLowerCase()).indexOf(noDiacritics(filterText.toLocaleLowerCase())) > -1) {
      hasFilterText = true;
      return hasFilterText;
    }
  });
  return hasFilterText;
};

export const filterAbstract = (project, filterText) => {
  let hasFilterText = false;
  const listFilterText = filterText.split(',');
  if (project.abstract) {
    listFilterText.forEach(filterText => {
      if (noDiacritics(project.abstract.toLocaleLowerCase()).indexOf(noDiacritics(filterText.toLocaleLowerCase())) > -1) {
        hasFilterText = true;
        return hasFilterText;
      }
    });
  }
  return hasFilterText;
};

export const filterRegion = (project, selectRegion) => {
  let hasRegion = false;
  let regions = project.area.regions;
  if (project.area.regions && project.area.regions.length > 0) {
    if (!(project.area.regions instanceof Array)) {
      regions = [project.area.regions];
    }
    regions.forEach(region => {
      selectRegion.forEach(select => {
        if (region.indexOf(select) > -1) {
          hasRegion = true;
          return hasRegion;
        }
      });
    });
  }
  return hasRegion;
};

export const filterDepartement = (project, selectDepartement) => {
  let hasDepartement = false;
  let departements = project.area.departements;
  if (project.area.departements && project.area.departements.length > 0) {
    if (!(project.area.departements instanceof Array)) {
      departements = [project.area.departements];
    }
    departements.forEach(departement => {
      selectDepartement.forEach(select => {
        if (noDiacritics(departement.toLocaleLowerCase()).indexOf(noDiacritics(select.toLocaleLowerCase())) > -1) {
          hasDepartement = true;
          return hasDepartement;
        }
      });
    });
  }
  return hasDepartement;
};

export const filterSourceur = (project, selectSourceur) => project.spider_name === selectSourceur;

export const filterMedia = (project, checkboxMedia) => {
  let hasMedia = false;
  let medias = project.video;
  if (project.video && project.video.length > 0) {
    if (!(project.video instanceof Array)) {
      medias = [project.video];
    }
    medias.forEach(media => {
      if (media.length > 0) {
        hasMedia = true;
        return hasMedia;
      }
    });
  }
  return hasMedia;
};
